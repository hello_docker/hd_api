'use strict';
 
const express = require('express');
const config = require('./config');

const os = require("os");
const hostname = os.hostname();
const ipinterface = os.networkInterfaces();
const hostip = (ipinterface.eth0||false)? ipinterface.eth0[0].address : '0.0.0.0';

const app = express();
const bodyParser = require('body-parser');
app.use(express.static('frontend'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const sql = require('./sqldb');

const retrysql = () => {
  sql.testDatabase( (testresult) => {
    if(testresult.err) setTimeout(retrysql,2000);
    console.dir(testresult);
  });
} 

retrysql();

// rest routes api
app.get('/api/get/all', (req,res) => {
  let allQuery = 'SELECT * FROM titletext ORDER BY id DESC LIMIT 10;';
  sql.query(allQuery, (err, rows, fields) => {
    if(err) {
      res.status(200).json(err);
    } else {
      res.status(200).json(rows);
    }
  });
  // res.status(200).json([{title:'hello world',text:'some text not sure how many char should be en'}]);
});

app.post('/api/get/all', (req,res) => {
  res.status(200).json([{title:'hello world',text:'some text not sure how many char should be en'}]);
});

app.post('/api/post', (req,res) => {
  sql.insertData(req.body, (err, rows, fields) => {
    if(err) {
      res.status(404).json(err);
    } else {
      res.status(200).json(rows);
    }
  });
});

// rest routes test
app.get('/hostdata', (req,res) => {
	res.status(200).json({hostname,hostip,buildversion:process.env.buildversion});
});

app.get('/test/truefalse/', (req,res) => {
  res.status(200).json({testTrue:true,testFalse:false});
});

app.get('/test/env/', (req,res) => {
  res.status(200).json(process.env);
});

app.listen(config.PORT, config.HOST);
console.log(`Running on http://${config.HOST}:${config.PORT}`);

module.exports = app; // for testing